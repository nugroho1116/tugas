<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class All_model extends CI_Model {

    public function getInfoUser()
    {
        $query = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        return $query;
    }

    public function get_id($id) {
        $query = $this->db->get_where('user', ['user_id' => $id])->row_array();
        return $query;
    }

    public function get_slider()
    {
        $query = $this->db->get('home')->result_array();
        return $query;
    }

    function delete_slider($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('home');
    }
    
    public function edit_slider($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('home', $data);
    }

    public function getImageBy_id($id = null)
    {
        $this->db->select('image');
        $this->db->where('id', $id);
        $query = $this->db->get('home');
        return $query->row_array();
    }

    public function get_content()
    {
        $query = $this->db->get('our_cycle')->result_array();
        return $query;
    }
    
    function delete_content($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('our_cycle');
    }

    public function edit_content($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('our_cycle', $data);
    }

    public function getImageBy_idContent($id = null)
    {
        $this->db->select('image');
        $this->db->where('id', $id);
        $query = $this->db->get('our_cycle');
        return $query->row_array();
    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");

class Admin extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		// Load model
        $this->load->model('All_model', 'model');
        // Cek session
        $this->_cek_session();
    }
    
	public function index()
	{
		$data['title'] = "Dashboard";
		$data['user'] = $this->model->getInfoUser();

		$this->load->view('admin/templates/header', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('admin/templates/footer', $data);
	}

	public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('user_id');
        redirect('front');

    }

	public function home($action = null, $id = null)
	{
		$data['title'] = "Home";
		$data['user'] = $this->model->getInfoUser();
		$data['slider'] = $this->model->get_slider();
		
		switch ($action) {
        case null :
			$this->load->view('admin/templates/header', $data);
			$this->load->view('admin/home', $data);
			$this->load->view('admin/templates/footer', $data);
            break;
        case 'add':
            $this->_addSlider(); 
            break;
        case 'edit':
            $this->_editSlider($id); 
            break;
        case 'delete':
            // Delete by id
            $this->model->delete_slider($id);
          	$this->session->set_flashdata('message', '<div class="alert alert-light alert-dismissible show fade"><i class="bi bi-check-circle"></i>
                            Delete Succes.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>');
            redirect('admin/home');
            break;
        default:
            $this->load->view('errors/error405');
            break;
      }
	}

	public function our_cycle($action = null, $id = null)
	{
		$data['title'] = "Our Cycle";
		$data['user'] = $this->model->getInfoUser();
		$data['content'] = $this->model->get_content();
		
		switch ($action) {
        case null :
			$this->load->view('admin/templates/header', $data);
			$this->load->view('admin/our-cycle', $data);
			$this->load->view('admin/templates/footer', $data);
            break;
        case 'add':
            $this->_addContent(); 
            break;
        case 'edit':
            $this-> _editContent($id); 
            break;
        case 'delete':
            // Delete by id
            $this->model->delete_content($id);
          	$this->session->set_flashdata('message', '<div class="alert alert-light alert-dismissible show fade"><i class="bi bi-check-circle"></i>
                            Delete Succes.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>');
            redirect('admin/our_cycle');
            break;
        default:
            $this->load->view('errors/error405');
            break;
      }
	}

	private function _addSlider()
    {
		$data['title'] = "Home";
        $data['user'] = $this->model->getInfoUser();

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
        $this->form_validation->set_rules('tag', 'Tag', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
			$this->load->view('admin/home', $data);
			$this->load->view('admin/templates/footer', $data);
        } else {
			// Time
            $current_datetime = date('Y-m-d H:i:s');
            //cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '100000';
                $config['upload_path'] = 'assets/uploads/slider';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {

                    $image = $this->upload->data('file_name');

                } else {
                    echo $this->upload->display_errors();
                }
            }
            // $current_datetime = date('Y-m-d H:i:s');

            $data = [
                'name' => $this->input->post('name', true),
                'deskripsi' => $this->input->post('deskripsi', true),
                'tag' => $this->input->post('tag', true),
                'image' => $image,
				'created_at' =>  $current_datetime,
				'updated_at' =>  $current_datetime,
            ];
			// Insert data
            $this->db->insert('home', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-light alert-dismissible show fade"><i class="bi bi-check-circle"></i>
                            Add Item Succes.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>');
            redirect('admin/home');

        }
    }

    private function _editSlider($id)
    {
		$data['title'] = "Home";
        $data['user'] = $this->model->getInfoUser();

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
        $this->form_validation->set_rules('tag', 'Tag', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
			$this->load->view('admin/home', $data);
			$this->load->view('admin/templates/footer', $data);
        } else {
			// Time
            $current_datetime = date('Y-m-d H:i:s');
            //cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '100000';
                $config['upload_path'] = 'assets/uploads/slider';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {

                    $image = $this->upload->data('file_name');

                } else {
                    echo $this->upload->display_errors();
                }
                $data = [
                    'name' => $this->input->post('name', true),
                    'deskripsi' => $this->input->post('deskripsi', true),
                    'tag' => $this->input->post('tag', true),
                    'image' => $image,
			        'created_at' =>  $current_datetime,
			        'updated_at' =>  $current_datetime,
                ];
                // Delete file directory
                $data_image = $this->model->getImageBy_id($id);
                unlink('assets/uploads/slider/' . $data_image['image']); 
                // Update data 
                $this->model->edit_slider($id, $data);
                $this->session->set_flashdata('message', '<div class="alert alert-light alert-dismissible show fade"><i class="bi bi-check-circle"></i>
                                Add Item Succes.
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>');
                redirect('admin/home');
            } else {
                $data = [
                    'name' => $this->input->post('name', true),
                    'deskripsi' => $this->input->post('deskripsi', true),
                    'tag' => $this->input->post('tag', true),
			        'updated_at' =>  $current_datetime,
                ];
                // Update data 
                $this->model->edit_slider($id, $data);
                $this->session->set_flashdata('message', '<div class="alert alert-light alert-dismissible show fade"><i class="bi bi-check-circle"></i>
                                Add Item Succes.
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>');
                redirect('admin/home');
            }
        }
    }

	private function _addContent()
    {
		$data['title'] = "Our Cycle";
        $data['user'] = $this->model->getInfoUser();

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
        $this->form_validation->set_rules('price', 'Price', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
			$this->load->view('admin/our-cycle', $data);
			$this->load->view('admin/templates/footer', $data);
        } else {
			// Time
            $current_datetime = date('Y-m-d H:i:s');
            //cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '100000';
                $config['upload_path'] = 'assets/uploads/content';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {

                    $image = $this->upload->data('file_name');

                } else {
                    echo $this->upload->display_errors();
                }
            }
            // $current_datetime = date('Y-m-d H:i:s');

            $data = [
                'name' => $this->input->post('name', true),
                'deskripsi' => $this->input->post('deskripsi', true),
                'price' => $this->input->post('price', true),
                'image' => $image,
				'updated_at' =>  $current_datetime,
            ];
			// Insert data
            $this->db->insert('our_cycle', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-light alert-dismissible show fade"><i class="bi bi-check-circle"></i>
                            Add Item Succes.
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>');
            redirect('admin/our_cycle');

        }
    }

	private function _editContent($id)
    {
		$data['title'] = "Our Cycle";
        $data['user'] = $this->model->getInfoUser();

        $this->form_validation->set_rules('name', 'Name', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
        $this->form_validation->set_rules('price', 'Price', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
			$this->load->view('admin/our-cycle', $data);
			$this->load->view('admin/templates/footer', $data);
        } else {
			// Time
            $current_datetime = date('Y-m-d H:i:s');
            //cek jika ada gambar yang akan diupload
            $upload_image = $_FILES['image']['name'];

            if ($upload_image) {
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '100000';
                $config['upload_path'] = 'assets/uploads/content';

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('image')) {

                    $image = $this->upload->data('file_name');

                } else {
                    echo $this->upload->display_errors();
                }
                $data = [
                    'name' => $this->input->post('name', true),
                    'deskripsi' => $this->input->post('deskripsi', true),
                    'price' => $this->input->post('price', true),
                    'image' => $image,
			        'updated_at' =>  $current_datetime,
                ];
                // Delete file directory
                $data_image = $this->model->getImageBy_idContent($id);
                unlink('assets/uploads/content/' . $data_image['image']); 
                // Update data 
                $this->model->edit_slider($id, $data);
                $this->session->set_flashdata('message', '<div class="alert alert-light alert-dismissible show fade"><i class="bi bi-check-circle"></i>
                                Add Item Succes.
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>');
                redirect('admin/our_cycle');
            } else {
                $data = [
                    'name' => $this->input->post('name', true),
                    'deskripsi' => $this->input->post('deskripsi', true),
                    'price' => $this->input->post('price', true),
			        'updated_at' =>  $current_datetime,
                ];
                // Update data 
                $this->model->edit_content($id, $data);
                $this->session->set_flashdata('message', '<div class="alert alert-light alert-dismissible show fade"><i class="bi bi-check-circle"></i>
                                Add Item Succes.
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>');
                redirect('admin/our_cycle');
            }
        }
    }

    private function _cek_session()
    {
        if ($this->session->userdata('username')) {
            return true;
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Silakan Login kembali</div>');
            redirect('auth');
        }
    }
}
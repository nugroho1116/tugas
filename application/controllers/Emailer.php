<?php defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");

class Emailer extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		// Load model
        $this->load->model('All_model', 'model');
        $this->load->helper('url');

    }

    public function index()
    {
        $data['title'] = "Forgot Password";

        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('forgot-password/send-email', $data);
        } else {
            $email = $this->input->post('email');
            $email_config = Array(
                'protocol'  => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => '465',
                'smtp_user' => 'nugrohodwi158@gmail.com',
                'smtp_pass' => 'lsbgdxsiplcloofw',
                'mailtype'  => 'html',
                'starttls'  => true,
                'newline'   => "\r\n"
            );
            $this->load->library('email', $email_config);
            $this->email->from('nugrohodwi158@gmail.com', 'invoice');
            $this->email->to($email);
            $this->email->subject('Reset Your Password');
            $message = "<p>Anda melakukan permintaan reset password</p>";
			$message .= "<a href='".site_url('emailer/reset_password')."'>klik reset password</a>";
            $this->email->message($message );
            $user = $this->db->get_where('user', ['email' => $email])->row_array();

            if ($user) {
                if ($this->email->send()) {
                    echo "Silakan cek email anda, <a href='https://mail.google.com/'>Klik</a>";
                } else {
                    echo "Send email gagal, <a href='http://localhost/Tugas/emailer'>Kembali</a>";
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email not registrasi</div>');
                redirect('emailer');
            }
        }  
    }

    public function reset_password()
    {
        $data['title'] = "Forgot Password";
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        $this->form_validation->set_rules('confirpassword', 'Confirpassword', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('forgot-password/reset-password', $data);
        } else{
            $username = $this->input->post('username');   
            $password = $this->input->post('password');   
            $confirpassword = $this->input->post('confirpassword');
            
            // Time
            $current_datetime = date('Y-m-d H:i:s');
            // Mencari username yang di input ada atau tidak di database
            $user = $this->db->get_where('user', ['username' => $username])->row_array();
            $id = $user['id']; 
            //jika usernya ada
            if ($user) {
                //cek password
                if ($password == $confirpassword) {
                    $data = [
                        'password' =>  password_hash($password, PASSWORD_DEFAULT),
                        'updated_at' =>  $current_datetime,
                    ];
                    $this->db->where('id', $id);
                    $this->db->update('user', $data);
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Reset password Success</div>');
                    redirect('auth');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Wrong confirm password</div>');
                    redirect('emailer/reset_password');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Username is not Registrasi!</div>');
                redirect('emailer/reset_password');
            }
        }

    }
    
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");

class Setting extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		// Load model
        $this->load->model('All_model', 'model');
        // Time
        // $current_datetime = date('Y-m-d H:i:s');

    }
    
    public function index($action = null, $id = null)
    {
      switch ($action) {
        case null :
            $data['title'] = "My Account Setting";
            $data['user'] = $this->model->getInfoUser();
            
            $this->load->view('admin/templates/header', $data);
		    $this->load->view('my-account/account-setting', $data);
		    $this->load->view('admin/templates/footer', $data);
            break;
        case 'edit-profile':
            $this->_editProfile(); 
            break;
        case 'change-password':
            $this->_changePassword(); 
            break;
        default:
            $this->load->view('errors/error405');
            break;
      }
    }

    private function _editProfile()
    {
        $data['title'] = "My Account Setting";
        $data['user'] = $this->model->getInfoUser();

        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
		    $this->load->view('my-account/account-setting', $data);
		    $this->load->view('admin/templates/footer', $data);
        } else {
            // Time
            $current_datetime = date('Y-m-d H:i:s');
            // Get id user login
            $id = $this->model->getInfoUser();
            $data = array (
                'username' => $this->input->post('username', true),
                'updated_at' =>  $current_datetime,
            );
            $this->session->set_flashdata('message-1', '<div class="alert alert-success" role="alert">Edit Profile Success</div>');
            $this->session->set_userdata($data);
            // Update edit profile
            $this->db->update('user', $data, $id);
            redirect('setting');
        }
    }

    private function _changePassword()
    {
        $data['title'] = "My Account Setting";
        $data['user'] = $this->model->getInfoUser();
        
        // Validasi data
        $this->form_validation->set_rules('currentpassword', 'Currentpassword', 'required|trim|callback_password_check');
        $this->form_validation->set_rules('newpassword', 'Newpassword', 'required|trim');
        $this->form_validation->set_rules('confirmpassword', 'Confirmpassword', 'required|trim');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/templates/header', $data);
		    $this->load->view('my-account/account-setting', $data);
		    $this->load->view('admin/templates/footer', $data);
        } else {
            // Time
            $current_datetime = date('Y-m-d H:i:s');
            // Get id user login
            $id = $this->model->getInfoUser();
            // Data input post
		    $new_pass = $this->input->post('newpassword');
		    $confirm_pass = $this->input->post('confirmpassword');
            
            // Update password
            if ($new_pass == $confirm_pass) {
                    $data = [
                       'password' => password_hash($this->input->post('newpassword'), PASSWORD_DEFAULT),
                       'updated_at' =>  $current_datetime,
                    ];
                    $this->session->set_flashdata('message-2', '<div class="alert alert-success" role="alert">Change Password Success</div>');
                    $this->db->update('user', $data, $id);
                    redirect('setting');
            } else {
                $this->session->set_flashdata('message-2', '<div class="alert alert-danger" role="alert">Confirm Password Wrong</div>');
                redirect('setting');
            }
        }
	}

    public function password_check($str)
    {
        $get = $this->session->all_userdata();
        $id = $get['user_id'];
        $user = $this->model->get_id($id);

        if (password_verify($str, $user['password'])) {
            return true;
        } else {
            return false;
        }
    }
}
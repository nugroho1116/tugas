<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("Asia/Jakarta");

class Auth extends CI_Controller {

     public function __construct()
    {
        parent::__construct();
        // Load helper 
        $this->load->helper('captcha');
       
    }

	public function index()
	{
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        $this->form_validation->set_rules('captcha', 'Captcha', 'required|trim|callback_check_captcha');
        if ($this->form_validation->run() == false) {
            $data['captcha'] = $this->_create_captcha();
            $this->load->view('login', $data);
        } else {
            //validasinya success
            $this->_login();
        }

	}

    private function _create_captcha()
    {
        $options = array(
            'img_path'      => FCPATH.'assets/uploads/captcha/',
            'img_url'       => base_url().'assets/uploads/captcha/',
            'font_path'     => BASEPATH.'system/fonts/texb.ttf',
            'img_width'     => 150,
            'img_height'    => 50,
            'expiration'    => 7200,
            'word_length'   => 6,
            'font_size'     => 25,
            'colors'        => array(
                'background' => array(171, 194, 177),
                'border' => array(10, 51, 11),
                'text' => array(0, 0, 0),
                'grid' => array(185, 234, 237)
            )
        );
        $captcha = create_captcha($options);
        // Unset previous captcha and set new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode', $captcha['word']);
        // Pass captcha image to view
        $captcha = $captcha['image'];
        return $captcha;
    }

    public function check_captcha()
    {
        if ($this->input->post('captcha') == $this->session->userdata('captchaCode')) {
            return true;
        } else {
            $this->form_validation->set_message('check_captcha', '<div class="alert alert-danger" role="alert">Wrong Captcha</div>');
            return false;
        } 
    }

    private function _login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->db->get_where('user', ['username' => $username])->row_array();

        //jika usernya ada
        if ($user) {
            //cek password
            if (password_verify($password, $user['password'])) {
                $data = [
                    'username' => $user['username'],
                    'user_id' => $user['user_id'],
                ];
                $this->session->set_userdata($data);
                if ($user['user_id'] == 1) {
                    redirect('admin');
                } else {
                    redirect('user');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Wrong password</div>');
                redirect('auth');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Username is not Registrasi!</div>');
            redirect('auth');
        }
    }
}
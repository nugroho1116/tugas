<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		// Load model
        $this->load->model('All_model', 'model');

    }

    public function index()
    {
        $data['slider'] = $this->model->get_slider();
        $data['content'] = $this->model->get_content();
        // var_dump($data);
        
        $this->load->view('templates/header', $data);
		$this->load->view('index');
		$this->load->view('templates/footer');
    }

}
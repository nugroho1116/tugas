<div class="content-wrapper container">

    <div class="page-heading">
        <h3>My Account Setting</h3>
    </div>
    <div class="page-content">
        <section class="row">
            <div class="container light-style flex-grow-1 container-p-y">
                <div class="card overflow-hidden">
                    <div class="row no-gutters row-bordered row-border-light">
                        <div class="col-md-3 pt-0">
                            <div class="list-group list-group-flush account-settings-links">
                                <a class="list-group-item list-group-item-action active" data-toggle="list"
                                    href="#account-general">General</a>
                                <a class="list-group-item list-group-item-action" data-toggle="list"
                                    href="#account-change-password">Change password</a>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="tab-content">
                                <div class="tab-pane fade active show" id="account-general">
                                    <form method="POST" action="<?= base_url('setting/index/edit-profile');?>">
                                        <?=$this->session->flashdata('message-1');?>
                                        <div class="card-body media align-items-center">
                                            <img src="<?=base_url('assets/uploads/images/') . $user['image']?>" alt=""
                                                class="d-block ui-w-80">
                                            <div class="media-body ml-4">
                                                <label class="btn btn-outline-primary">
                                                    Upload new photo
                                                    <input type="file" name="images" class="account-settings-fileinput">
                                                </label> &nbsp;
                                                <div class="text-light small mt-1">Allowed JPG, GIF or PNG. Max size of
                                                    800K
                                                </div>
                                            </div>
                                        </div>
                                        <hr class="border-light m-0">

                                        <div class="card-body">
                                            <div class="form-group">
                                                <label class="form-label">Username</label>
                                                <small class="text-danger"><?=form_error('username');?></small>
                                                <input type="text" class="form-control mb-1"
                                                    value="<?= $user['username'] ?>" name="username">
                                            </div>
                                        </div>
                                        <div class="text-right mt-3">
                                            <button type="submit" id="submit" class="btn btn-primary">Save
                                                changes</button>&nbsp;
                                            <a class="btn btn-default" href="<?= base_url('admin')?>"
                                                role="button">Cancel</a>
                                        </div>
                                        <br />
                                    </form>
                                </div>

                                <div class="tab-pane fade" id="account-change-password">
                                    <form method="POST" action="<?= base_url('setting/index/change-password');?>">
                                        <?=$this->session->flashdata('message-2');?>
                                        <div class="card-body pb-2">

                                            <div class=" form-group">
                                                <label class="form-label">Current password</label>
                                                <small class="text-danger"><?=form_error('currentpassword');?></small>
                                                <input type="password" class="form-control" name="currentpassword">
                                            </div>
                                            <div class=" form-group">
                                                <label class="form-label">New password</label>
                                                <small class="text-danger"><?=form_error('newpassword');?></small>
                                                <input type="password" class="form-control" name="newpassword">
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label">Confirm password</label>
                                                <small class="text-danger"><?=form_error('confirmpassword');?></small>
                                                <input type="password" class="form-control" name="confirmpassword">
                                            </div>
                                            <br />
                                            <div class="text-right mt-3">
                                                <button type="submit" id="submit" class="btn btn-primary">Save
                                                    changes</button>&nbsp;
                                                <a class="btn btn-default" href="<?= base_url('admin')?>"
                                                    role="button">Cancel</a>
                                            </div>
                                            <br />
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </section>
</div>

</div>
<br />
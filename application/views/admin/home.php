<div class="content-wrapper container">

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3>Home</h3>
                    <p class="text-subtitle text-muted">The default layout</p>
                </div>
                <div class="col-12 col-md-6 order-md-2 order-first">
                    <nav aria-label="breadcrumb" class="breadcrumb-header float-start float-lg-end">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="index.html">Menu</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">
                                Home
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <section class="section">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Upload Slider</h4>
                    <div class=" d-flex mb-8">
                        <!-- Button trigger modal -->
                        <button type=" button" class="btn icon btn-sm icon-left btn-primary" data-bs-toggle="modal"
                            data-bs-target="#add">
                            <i class="bi bi-plus-circle"></i> Add
                        </button>

                        <!-- Modal -->
                        <form method="post" action="<?=base_url('admin/home/add');?>" enctype="multipart/form-data">
                            <div class=" modal fade" id="add" tabindex="-1" aria-labelledby="exampleModalLabel"
                                aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class=" modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Form add data
                                            </h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="mb-3">
                                                <label for="exampleInputEmail1" class="form-label">Name</label>
                                                <small class="text-danger"><?=form_error('name');?></small>
                                                <input type="text" class="form-control" name="name"
                                                    id="exampleInputEmail1" aria-describedby="emailHelp">
                                            </div>
                                            <div class="mb-3">
                                                <label for="exampleInputEmail1" class="form-label">Deskripsi</label>
                                                <small class="text-danger"><?=form_error('deskripsi');?></small>
                                                <input type="text" class="form-control" name="deskripsi"
                                                    id="exampleInputEmail1" aria-describedby="emailHelp">
                                            </div>
                                            <div class="mb-3">
                                                <label for="exampleInputPassword1" class="form-label">Tag</label>
                                                <small class="text-danger"><?=form_error('tag');?></small>
                                                <input type="text" class="form-control" name="tag"
                                                    id="exampleInputPassword1">
                                            </div>
                                            <div class="mb-3">
                                                <label for="exampleInputPassword1" class="form-label">Image</label>
                                                <input type="file" class="form-control" name="image"
                                                    id="exampleInputPassword1">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Deskripsi</th>
                                <th scope="col">Tag</th>
                                <th scope="col">Created_at</th>
                                <th scope="col">Updated_at</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?=$this->session->flashdata('message');?>
                            <?php if (!empty($slider)) {$n = 0;
    foreach ($slider as $i) {$n++;
        ?>
                            <tr>
                                <td><?= $n; ?></td>
                                <td><?= $i['name']; ?></td>
                                <td>
                                    <p><?= $i['deskripsi']; ?></p>
                                </td>
                                <td><?= $i['tag']; ?></td>
                                <td><?= $i['created_at']; ?></td>
                                <td><?= $i['updated_at']; ?></td>
                                <td>
                                    <div class=" buttons">


                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn icon btn-sm btn-primary" data-bs-toggle="modal"
                                            data-bs-target="#edit<?= $i['id']; ?>">
                                            <i class="bi bi-pencil"></i>
                                        </button>

                                        <!-- Modal -->
                                        <form enctype="multipart/form-data"
                                            action="<?= base_url('admin/home/edit/' . $i['id']); ?>" method="POST">
                                            <div class="modal fade" id="edit<?= $i['id']; ?>" tabindex="-1"
                                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Edit
                                                            </h1>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="mb-3">
                                                                <label for="exampleInputEmail1"
                                                                    class="form-label">Name</label>
                                                                <small
                                                                    class="text-danger"><?=form_error('name');?></small>
                                                                <input type="text" class="form-control" name="name"
                                                                    id="exampleInputEmail1" aria-describedby="emailHelp"
                                                                    value="<?= $i['name']; ?>">
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="exampleInputEmail1"
                                                                    class="form-label">Deskripsi</label>
                                                                <small
                                                                    class="text-danger"><?=form_error('deskripsi');?></small>
                                                                <input type="text" class="form-control" name="deskripsi"
                                                                    id="exampleInputEmail1" aria-describedby="emailHelp"
                                                                    value="<?= $i['deskripsi']; ?>">
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="exampleInputPassword1"
                                                                    class="form-label">Tag</label>
                                                                <small
                                                                    class="text-danger"><?=form_error('tag');?></small>
                                                                <input type="text" class="form-control" name="tag"
                                                                    id="exampleInputPassword1"
                                                                    value="<?= $i['tag']; ?>">
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="exampleInputPassword1"
                                                                    class="form-label">Image</label>
                                                                <input type="file" class="form-control" name="image"
                                                                    id="exampleInputPassword1">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Save
                                                                changes</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- End Modal -->

                                        <a href="<?= base_url('admin/home/delete/' . $i['id']); ?>"
                                            class="btn icon btn-sm btn-danger"
                                            onclick="return confirm('Are you sure to delete data?')?true:false;">
                                            <i class="bi bi-trash3-fill"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php }}?>

                            <?php ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

</div>
<footer>
    <div class="container">
        <div class="footer clearfix mb-0 text-muted">
            <div class="float-start">
                <p>2021 &copy; Mazer</p>
            </div>
            <div class="float-end">
                <p>Crafted with <span class="text-danger"><i class="bi bi-heart"></i></span> by <a
                        href="https://saugi.me">Saugi</a></p>
            </div>
        </div>
    </div>
</footer>
</div>
</div>
<script src="<?= base_url() ?>assets/vendor/adminMazer/assets/js/bootstrap.js"></script>
<script src="<?= base_url() ?>assets/vendor/adminMazer/assets/js/app.js"></script>
<script src="<?= base_url() ?>assets/vendor/adminMazer/assets/js/pages/horizontal-layout.js"></script>
<script src="<?= base_url() ?>assets/vendor/adminMazer/assets/extensions/apexcharts/apexcharts.min.js"></script>
<script src="<?= base_url() ?>assets/vendor/adminMazer/assets/js/pages/dashboard.js"></script>
<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous">
</script>

</body>

</html>
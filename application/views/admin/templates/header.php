<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ?></title>

    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/adminMazer/assets/css/main/app.css">
    <link rel="shortcut icon" href="<?= base_url() ?>assets/vendor/adminMazer/assets/images/logo/favicon.svg"
        type="image/x-icon">
    <link rel="shortcut icon" href="<?= base_url() ?>assets/vendor/adminMazer/assets/images/logo/favicon.png"
        type="image/png">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style2.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/adminMazer/assets/css/shared/iconly.css">


</head>

<body>
    <div id="app">
        <div id="main" class="layout-horizontal">
            <header class="mb-5">
                <div class="header-top">
                    <div class="container">
                        <div class="logo">
                            <a><img src="<?= base_url() ?>assets/vendor/adminMazer/assets/images/logo/logo.svg"
                                    alt="Logo"></a>
                        </div>
                        <div class="header-top-right">

                            <div class="dropdown">
                                <a href="#" id="topbarUserDropdown"
                                    class="user-dropdown d-flex align-items-center dropend dropdown-toggle "
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    <div class="avatar avatar-md2">
                                        <img src="<?=base_url('assets/uploads/images/') . $user['image']?>"
                                            alt="Avatar">
                                    </div>
                                    <div class="text">
                                        <h6 class="user-dropdown-name"><?= $user['username']?></h6>
                                        <p class="user-dropdown-status text-sm text-muted">Admin</p>
                                    </div>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end shadow-lg"
                                    aria-labelledby="topbarUserDropdown">
                                    <li><a class="dropdown-item" href="<?= base_url('setting')?>">My Account Setting</a>
                                    </li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                    <li><a class="dropdown-item" href="<?= base_url('admin/logout')?>">Logout</a></li>
                                </ul>
                            </div>

                            <!-- Burger button responsive -->
                            <a href="#" class="burger-btn d-block d-xl-none">
                                <i class="bi bi-justify fs-3"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <nav class="main-navbar">
                    <div class="container">
                        <ul>



                            <li class="menu-item  ">
                                <a href="<?= base_url('admin')?>" class='menu-link'>
                                    <i class="bi bi-grid-fill"></i>
                                    <span>Dashboard</span>
                                </a>
                            </li>



                            <li class="menu-item  has-sub">
                                <a href="#" class='menu-link'>
                                    <i class="bi bi-stack"></i>
                                    <span>Menu</span>
                                </a>
                                <div class="submenu ">
                                    <!-- Wrap to submenu-group-wrapper if you want 3-level submenu. Otherwise remove it. -->
                                    <div class="submenu-group-wrapper">


                                        <ul class="submenu-group">

                                            <li class="submenu-item  ">
                                                <a href="<?= base_url('admin/home')?>" class='submenu-link'>Home</a>


                                            </li>



                                            <li class="submenu-item  ">
                                                <a href="<?= base_url('admin/our_cycle')?>" class='submenu-link'>Our
                                                    cycle</a>


                                            </li>


                                        </ul>


                                    </div>
                                </div>
                            </li>



                            <li class="menu-item active has-sub">
                                <a href="#" class='menu-link'>
                                    <i class="bi bi-grid-1x2-fill"></i>
                                    <span>Layouts</span>
                                </a>
                                <div class="submenu ">
                                    <!-- Wrap to submenu-group-wrapper if you want 3-level submenu. Otherwise remove it. -->
                                    <div class="submenu-group-wrapper">


                                        <ul class="submenu-group">

                                            <li class="submenu-item  ">
                                                <a href="layout-default.html" class='submenu-link'>Default Layout</a>


                                            </li>



                                            <li class="submenu-item  ">
                                                <a href="layout-vertical-1-column.html" class='submenu-link'>1
                                                    Column</a>


                                            </li>



                                            <li class="submenu-item  ">
                                                <a href="layout-vertical-navbar.html" class='submenu-link'>Vertical
                                                    Navbar</a>


                                            </li>



                                            <li class="submenu-item  ">
                                                <a href="layout-rtl.html" class='submenu-link'>RTL Layout</a>


                                            </li>



                                            <li class="submenu-item active ">
                                                <a href="layout-horizontal.html" class='submenu-link'>Horizontal
                                                    Menu</a>


                                            </li>

                                        </ul>


                                    </div>
                                </div>
                            </li>

                    </div>
                </nav>

            </header>
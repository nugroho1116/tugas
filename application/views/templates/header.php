<!DOCTYPE html>
<html lang="en">

<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Cycle</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/vendor/Cycle/css/bootstrap.min.css">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/vendor/Cycle/css/style.css">
    <!-- Responsive-->
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/Cycle/css/responsive.css">
    <!-- fevicon -->
    <link rel="icon" href="<?= base_url() ?>assets/vendor/Cycle/images/fevicon.png" type="image/gif" />
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/Cycle/css/jquery.mCustomScrollbar.min.css">
    <!-- Tweaks for older IEs-->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <!-- owl stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,700|Raleway:400,700,800&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/Cycle/css/owl.carousel.min.css">
    <link rel="stylesoeet" href="<?= base_url() ?>assets/vendor/Cycle/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css"
        media="screen">
</head>

<body>
    <!-- header section start -->
    <div class="header_section header_bg">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a href="<?= base_url('index.php') ?>" class="logo"><img
                    src="<?= base_url() ?>assets/vendor/Cycle/images/logo.png"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.html">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.html">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cycle.html">Our Cycle</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="shop.html">Shop</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="news.html">News</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact.html">Contact Us</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <div class="login_menu">
                        <ul>
                            <li><a href="<?= base_url('auth/index') ?>">Login</a></li>
                            <li><a href="#"><img src="<?= base_url() ?>assets/vendor/Cycle/images/trolly-icon.png"></a>
                            </li>
                            <li><a href="#"><img src="<?= base_url() ?>assets/vendor/Cycle/images/search-icon.png"></a>
                            </li>
                        </ul>
                    </div>
                    <div></div>
                </form>
            </div>
            <div id="main">
                <span style="font-size:36px;cursor:pointer; color: #fff" onclick="openNav()"><img
                        src="<?= base_url() ?>assets/vendor/Cycle/images/toggle-icon.png" style="height: 30px;"></span>
            </div>
        </nav>
        <!-- banner section start -->
        <div class="banner_section layout_padding">
            <div id="main_slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <?php foreach($slider as $a): ?>
                    <div class="carousel-item">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="best_text"><?= $a['tag']?></div>
                                    <div class="image_1"><img
                                            src="<?=base_url('assets/uploads/slider/') . $a['image'];?>"></div>
                                </div>
                                <div class="col-md-5">
                                    <h1 class="banner_taital"><?= $a['name']?></h1>
                                    <p class=" banner_text"><?= $a['deskripsi']?></p>
                                    <div class="contact_bt"><a href="contact.html">Shop Now</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                <a class="carousel-control-prev" href="#main_slider" role="button" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="carousel-control-next" href="#main_slider" role="button" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
        <!-- banner section end -->
    </div>
    <!-- header section end -->
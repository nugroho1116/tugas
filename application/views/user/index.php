<!-- cycle section start -->
<div class="cycle_section layout_padding">
    <div class="container">
        <h1 class="cycle_taital">Our cycle</h1>
        <p class="cycle_text">It is a long established fact that a reader will be distracted by the </p>
        <div class="cycle_section_2 layout_padding">
            <div class="row">
                <div class="col-md-6">
                    <div class="box_main">
                        <h6 class="number_text">01</h6>
                        <div class="image_2"><img src="<?= base_url() ?>assets/vendor/Cycle/images/img-2.png"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h1 class="cycles_text">Cycles</h1>
                    <p class="lorem_text">It is a long established fact that a reader will be distracted by the
                        readable content of a page when looking at its layout. The point of using Lorem Ipsum is
                        that it has a more-or-less normal distribution of letters</p>
                    <div class="btn_main">
                        <div class="buy_bt"><a href="#">Buy Now</a></div>
                        <h4 class="price_text">Price <span style=" color: #f7c17b">$</span> <span
                                style=" color: #325662">200</span></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="cycle_section_3 layout_padding">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="cycles_text">Stylis Cycle</h1>
                    <p class="lorem_text">It is a long established fact that a reader will be distracted by the
                        readable content of a page when looking at its layout. The point of using Lorem Ipsum is
                        that it has a more-or-less normal distribution of letters</p>
                    <div class="btn_main">
                        <div class="buy_bt"><a href="#">Buy Now</a></div>
                        <h4 class="price_text">Price <span style=" color: #f7c17b">$</span> <span
                                style=" color: #325662">200</span></h4>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box_main_3">
                        <h6 class="number_text_2">02</h6>
                        <div class="image_2"><img src="<?= base_url() ?>assets/vendor/Cycle/images/img-3.png"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="cycle_section_2 layout_padding">
            <div class="row">
                <div class="col-md-6">
                    <div class="box_main_3">
                        <h6 class="number_text_2">03</h6>
                        <div class="image_2"><img src="<?= base_url() ?>assets/vendor/Cycle/images/img-4.png"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h1 class="cycles_text">Mordern <br>Cycle</h1>
                    <p class="lorem_text">It is a long established fact that a reader will be distracted by the
                        readable content of a page when looking at its layout. The point of using Lorem Ipsum is
                        that it has a more-or-less normal distribution of letters</p>
                    <div class="btn_main">
                        <div class="buy_bt"><a href="#">Buy Now</a></div>
                        <h4 class="price_text">Price <span style=" color: #f7c17b">$</span> <span
                                style=" color: #325662">200</span></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="read_btn_main">
            <div class="read_bt"><a href="#">Read More</a></div>
        </div>
    </div>
</div>
<!-- cycle section end -->

<!-- footer section start -->
<div class="footer_section layout_padding">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 col-sm-12 padding_0">
                <div class="map_main">
                    <div class="map-responsive">
                        <iframe
                            src="https://www.google.com/maps/embed/v1/place?key=AIzaSyA0s1a7phLN0iaD6-UE7m4qP-z21pH0eSc&amp;q=Eiffel+Tower+Paris+France"
                            width="600" height="400" frameborder="0" style="border:0; width: 100%;"
                            allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="call_text"><a href="#"><img
                            src="<?= base_url() ?>assets/vendor/Cycle/images/map-icon.png"><span
                            class="padding_left_0">Page
                            when looking at its layou</span></a></div>
                <div class="call_text"><a href="#"><img
                            src="<?= base_url() ?>assets/vendor/Cycle/images/call-icon.png"><span
                            class="padding_left_0">Call
                            Now +01 123467890</span></a></div>
                <div class="call_text"><a href="#"><img
                            src="<?= base_url() ?>assets/vendor/Cycle/images/mail-icon.png"><span
                            class="padding_left_0">demo@gmail.com</span></a></div>
                <div class="social_icon">
                    <ul>
                        <li><a href="#"><img src="<?= base_url() ?>assets/vendor/Cycle/images/fb-icon1.png"></a>
                        </li>
                        <li><a href="#"><img src="<?= base_url() ?>assets/vendor/Cycle/images/twitter-icon.png"></a>
                        </li>
                        <li><a href="#"><img src="<?= base_url() ?>assets/vendor/Cycle/images/linkedin-icon.png"></a>
                        </li>
                        <li><a href="#"><img src="<?= base_url() ?>assets/vendor/Cycle/images/instagram-icon.png"></a>
                        </li>
                    </ul>
                </div>
                <input type="text" class="email_text" placeholder="Enter Your Email" name="Enter Your Email">
                <div class="subscribe_bt"><a href="#">Subscribe</a></div>
            </div>
        </div>
    </div>
</div>
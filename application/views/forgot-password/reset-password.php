<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $title ;?></title>
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous"> -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style1.css">
</head>

<body>
    <div class="login_form">
        <form class="form" method="POST" action="<?=base_url('emailer/reset_password');?>"
            enctype="multipart/form-data">
            <h1 class=" form_title"> Reset Password </h1>
            <?=$this->session->flashdata('message');?>
            <br />
            <small class="text-danger"><?=form_error('username');?></small>
            <div class="form_div">
                <input type="text" class="form_input" placeholder=" " name="username"
                    value="<?=set_value('username');?>">
                <label class="form_label">Username</label>
            </div>
            <small class="text-danger"><?=form_error('password');?></small>
            <div class="form_div">
                <input type="password" class="form_input" placeholder=" " name="password"
                    value="<?=set_value('password');?>">
                <label class="form_label">New Password</label>
            </div>
            <small class="text-danger"><?=form_error('confirpassword');?></small>
            <div class="form_div">
                <input type="password" class="form_input" placeholder=" " name="confirpassword"
                    value="<?=set_value('confirpassword');?>">
                <label class="form_label">Confirmasi Password</label>
            </div>
            <button type="submit" class="form_button">Submit</button>
        </form>
    </div>



    <!-- <script src="<?= base_url() ?>assets/js/style1.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
        integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"
        integrity="sha384-fbbOQedDUMZZ5KreZpsbe1LCZPVmfTnH7ois6mU1QK+m14rQ1l2bGBq41eYeM/fS" crossorigin="anonymous">
    </script> -->
</body>

</html>